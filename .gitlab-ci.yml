image:
  name: registry.gitlab.com/gitlab-org/gitlab-build-images:terraform
  entrypoint:
    - '/usr/bin/env'
    - 'PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin'

variables:
  GITLAB_TF_ADDRESS: ${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/terraform/state/${CI_PROJECT_NAME}
  PLAN: plan.tfplan
  JSON_PLAN_FILE: tfplan.json

stages:
  - prepare
  - validate
  - build
  - deploy

.terraform-init:
  stage: prepare
  before_script:
    - cd ${TERRAFORM_DIRECTORY}
    - terraform --version
  script:
    - terraform init -backend-config="address=${GITLAB_TF_ADDRESS}-${TERRAFORM_DIRECTORY}" -backend-config="lock_address=${GITLAB_TF_ADDRESS}-${TERRAFORM_DIRECTORY}/lock" -backend-config="unlock_address=${GITLAB_TF_ADDRESS}-${TERRAFORM_DIRECTORY}/lock" -backend-config="username=${GITLAB_USER_LOGIN}" -backend-config="password=${GITLAB_API_TOKEN}" -backend-config="lock_method=POST" -backend-config="unlock_method=DELETE" -backend-config="retry_wait_min=5"
  cache:
    key: ${TERRAFORM_DIRECTORY}
    paths:
      - ${TERRAFORM_DIRECTORY}/.terraform/

.terraform-validate:
  stage: validate
  before_script:
    - cd ${TERRAFORM_DIRECTORY}
    - terraform --version
  script:
    - terraform validate
  cache:
    key: ${TERRAFORM_DIRECTORY}
    paths:
      - ${TERRAFORM_DIRECTORY}/.terraform/

.terraform-plan-generation:
  stage: build
  before_script:
    - cd ${TERRAFORM_DIRECTORY}
    - terraform --version
    - apk --no-cache add jq
    - alias convert_report="jq -r '([.resource_changes[]?.change.actions?]|flatten)|{\"create\":(map(select(.==\"create\"))|length),\"update\":(map(select(.==\"update\"))|length),\"delete\":(map(select(.==\"delete\"))|length)}'"
  script:
    - terraform plan -out=${PLAN}
    - terraform show --json ${PLAN} | convert_report > ${JSON_PLAN_FILE}
  artifacts:
    paths:
      - ${TERRAFORM_DIRECTORY}/${PLAN}
    reports:
      terraform: ${TERRAFORM_DIRECTORY}/${JSON_PLAN_FILE}
  cache:
    key: ${TERRAFORM_DIRECTORY}
    paths:
      - ${TERRAFORM_DIRECTORY}/.terraform/

.terraform-apply:
  stage: deploy
  before_script:
    - cd ${TERRAFORM_DIRECTORY}
    - terraform --version
  script:
    - terraform apply -input=false $PLAN
  cache:
    key: ${TERRAFORM_DIRECTORY}
    paths:
      - ${TERRAFORM_DIRECTORY}/.terraform/

review_init:
  extends: .terraform-init
  variables:
    TERRAFORM_DIRECTORY: "review"

staging_init:
  extends: .terraform-init
  variables:
    TERRAFORM_DIRECTORY: "staging"

production_init:
  extends: .terraform-init
  variables:
    TERRAFORM_DIRECTORY: "production"

review_validate:
  extends: .terraform-validate 
  variables:
    TERRAFORM_DIRECTORY: "review"
  dependencies:
    - review_init

staging_validate:
  extends: .terraform-validate 
  variables:
    TERRAFORM_DIRECTORY: "staging"
  dependencies:
    - staging_init

production_validate:
  extends: .terraform-validate 
  variables:
    TERRAFORM_DIRECTORY: "production"
  dependencies:
    - production_init

review_plan:
  extends: .terraform-plan-generation
  variables:
    TERRAFORM_DIRECTORY: "review"
  # Review will not include an artifact name

staging_plan:
  extends: .terraform-plan-generation
  variables:
    TERRAFORM_DIRECTORY: "staging"
  artifacts:
    name: Staging

production_plan:
  extends: .terraform-plan-generation
  variables:
    TERRAFORM_DIRECTORY: "production"
  artifacts:
    name: Production

# Separate apply job for manual launching Terraform as it can be destructive
# action.
staging_apply:
  extends: .terraform-apply
  environment:
    name: staging
  variables:
   TERRAFORM_DIRECTORY: "staging"
  dependencies:
    - staging_plan
  when: manual
  only:
    - master

production_apply:
  extends: .terraform-apply
  environment:
    name: production
  variables:
   TERRAFORM_DIRECTORY: "production" 
  dependencies:
    - production_plan
  when: manual
  only:
    - master

